origami-pdf (2.1.0-1) unstable; urgency=medium

  * Add patch to fix tests under ruby3.3
  * Bump Standards-Version to 4.7.0; no changes needed

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 27 Aug 2024 20:41:36 -0300

origami-pdf (2.1.0-1~exp2) unstable; urgency=medium

  * Team upload.

  [ Antonio Terceiro ]
  * origami-pdf: depend on ruby-origami from the same version
  * autopkgtest: add encrypt/decrypt test
  * Add patch to fix PDF encryption
  * autopkgtest: write to temporary dir
  * autopkgtest: add pdfmetadata test
  * autopkgtest: add pdfdecrypt test
  * Add patch from an upstream fork with fix for Ruby 3+ (Closes: #1004068)
  * Re-encrypt debian/tests/encrypted.pdf
  * autopkgtest: add roundtrip test (encrypt then decrypt)
  * debian/source: mention inclusion of test PDFs
  * Drop *-Ruby-Versions fields
  * debian/changelog: merge entries from unstable

  [ Daniel Leidert ]
  * Merge packaging changes from latest work on unstable

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 06 Feb 2023 18:25:55 +0100

origami-pdf (2.1.0-1~exp1) experimental; urgency=medium

  [ Antonio Terceiro ]
  * Remove myself from Uploaders:

  [ Cédric Boutillier ]
  * New upstream version 2.1.0
  * Refresh 0001-Adjust-paths-to-work-against-installed-code.patch
  * Do not recommend ruby-gtk2, suggest ruby-pdfwalker
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Use salsa.debian.org in Vcs-* fields
  * Move debian/watch to gemwatch.debian.net
  * Bump debhelper compatibility level to 11
  * Do not try to install bin/gui/* (moved from upstream to another gem)
  * Remove copyright paragraph about bin/gui.
  * Use https for copyright format URL
  * Set myself as Uploader
  * Remove unnecessary Testsuite declaration

 -- Cédric Boutillier <boutil@debian.org>  Wed, 09 Jan 2019 23:04:53 +0100

origami-pdf (2.0.4+dfsg-2) UNRELEASED; urgency=medium

  * Team upload.
  * d/patches/0003-Fix-superclass-mismatch-for-class-Socket-TypeError.patch:
    Add patch to workaround at least the TypeError (#911735 ). Now there is
    a ton of deprecations left (#911735).
  * d/patches/series: Enable new patch.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 26 Nov 2021 04:40:36 +0100

origami-pdf (2.0.4+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Removes Bignum/Fixnum usage (closes: #951248).

  [ Antonio Terceiro ]
  * Remove myself from Uploaders:

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * debian/control: Remove empty control field Uploaders.
  * Update watch file format version to 4.
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Remove unnecessary 'Testsuite: autopkgtest' header.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-colorize.
    + ruby-origami: Drop versioned constraint on ruby-colorize in Depends.

  [ Daniel Leidert ]
  * d/contol: Add Rules-Requires-Root field.
    (Standards-Version): Bump to 4.6.0.
    (Depends): Remove ruby-interpreter and use ${ruby:Depends}.
    (Uploaders): Added myself.
  * d/copyright: Add Upstream-Contact field.
    (Copyright): Add team.
  * d/origami-pdf.install: Don't install programs from bin/* directly.
  * d/rules: Use gem installation layout and move bin/* to origami-pdf.
  * d/watch: Update file, handle repackaging, and use Github.
  * d/patches/0002-Fix-Ruby-2.7-keyword-arguments-warnings.patch: Add patch.
    - Fixes Ruby 3.0 keyword argument handling error (closes: #996155).
  * d/patches/series: Add new patch.
  * d/upstream/metadata: Add missing fields.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 25 Nov 2021 23:04:48 +0100

origami-pdf (2.0.0-1) unstable; urgency=medium

  * New upstream release
    + Includes fix for superclass mismatch for class Date (Closes: #841820)
  * update packaging with a new run of `dh-make-ruby -w`
    + bump Standards-Version to 3.9.8, no changes needed
    + new dependency: ruby-colorize
    + update Homepage: field
    + refresh debian/rules to make gem2deb-test-runner check dependencies
      during build.
  * debian/patches/gemspec.patch: dropped, not needed anymore
  * debian/patches/dont_include_before_requiring.patch: dropped, not needed
    anymore
  * debian/tests/control:
    + point at debian-local signed PDF
    + pass --check-dependencies to gem2deb-test-runner
  * Change Section: to `utils` (Closes: #832247)

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 28 Oct 2016 17:21:34 -0200

origami-pdf (1.2.7-3) unstable; urgency=medium

  * dont_include_before_requiring.patch: fix incompatibility with Ruby 2.2
    (Closes: #806133)

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 03 Dec 2015 15:07:34 -0200

origami-pdf (1.2.7-2) unstable; urgency=medium

  * Rename source and main binary packages to origami-pdf. See #773197

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 15 Dec 2014 16:52:35 -0200

origami (1.2.7-1) unstable; urgency=medium

  * Initial release (Closes: #767911)

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 06 Nov 2014 18:06:05 -0200
